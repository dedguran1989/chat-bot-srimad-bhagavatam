require('dotenv').config();

module.exports = {
  apps: [
    {
      name: 'chat-bot',
      script: 'index.js',
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
        DATABASE_NAME: process.env.DATABASE_NAME,
        DATABASE_USER: process.env.DATABASE_USER,
        DATABASE_PASSWORD: process.env.DATABASE_PASSWORD,
        DATABASE_HOST: process.env.DATABASE_HOST,
        DATABASE_PORT: process.env.DATABASE_PORT,
      },
      env_production: {
        NODE_ENV: 'production',
        DATABASE_NAME: process.env.DATABASE_NAME,
        DATABASE_USER: process.env.DATABASE_USER,
        DATABASE_PASSWORD: process.env.DATABASE_PASSWORD,
        DATABASE_HOST: process.env.DATABASE_HOST,
        DATABASE_PORT: process.env.DATABASE_PORT,
      },
    },
  ],

  deploy: {
    production: {
      key: process.env.HOST_KEY,
      user: process.env.HOST_USER,
      host: process.env.HOST_ID,
      ref: 'origin/main',
      repo: 'https://gitlab.com/dedguran1989/chat-bot-srimad-bhagavatam.git',
      path: '/home/ubuntu/chat-bot-srimad-bhagavatam',

      // Шаги перед деплоем (создание дампа базы и копирование на сервер)
      'pre-deploy': [
        'echo "Creating database dump..."',
        'pg_dump -U ${DATABASE_USER} -h ${DATABASE_HOST} -p ${DATABASE_PORT} -Fc -f $(pwd)/veda.dump ${DATABASE_NAME}', // Дамп базы данных
        'scp -i TelegramBot.pem $(pwd)/veda.dump ${HOST_USER}@${HOST_ID}:/home/ubuntu/chat-bot-srimad-bhagavatam/source/veda.dump', // Копирование дампа на сервер
      ],

      // Шаги после деплоя (восстановление базы и другие операции)
      'post-deploy': [
        'echo "Restoring database on server..."',
        'ssh -i TelegramBot.pem ${HOST_USER}@${HOST_ID} "pg_restore -U ${DATABASE_USER} -h localhost -d ${DATABASE_NAME} -1 --clean --if-exists /home/ubuntu/chat-bot-srimad-bhagavatam/source/veda.dump"', // Восстановление базы данных на сервере
        'source ~/.nvm/nvm.sh && npm install --force', // Установка зависимостей
        'npx sequelize-cli db:migrate --env production', // Выполнение миграций
        'npx sequelize-cli db:seed:all --env production', // Запуск сеедеров
        'pm2 reload ecosystem.config.js --env production', // Перезагрузка приложения
      ],

      // Другие настройки
      'pre-setup': '',
      ssh_options: 'ForwardAgent=yes',
    },
  },
};
//
