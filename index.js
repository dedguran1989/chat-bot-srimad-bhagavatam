require('dotenv').config();
const { bot } = require('./config');
const {
  checkAnswer,
  askQuestion,
  handleNextQuestion,
} = require('./botFunctions');
const sequelize = require('./db');
const { getQuestions, User: UserModel } = require('./models');
const { Questions, Books } = require('./models'); // Предполагаем, что есть модель Books
const userAnswersMap = new Map();

const start = async () => {
  try {
    // Подключение к базе данных
    await sequelize.authenticate();
    await sequelize.sync();
    console.log('Подключение к БД успешно установлено.');
  } catch (e) {
    console.error('Ошибка подключения к БД:', e);
    return;
  }

  // Установка команд бота
  bot.setMyCommands([
    {
      command: '/start',
      description: 'Вы можете посмотреть все команды',
    },
    { command: '/info', description: 'Получить информацию о вашем прогрессе' },
    { command: '/test', description: 'Начать тестирование' },
  ]);

  // Обработка текстовых сообщений
  bot.on('message', async (msg) => {
    const text = msg.text;
    const chatId = msg.chat.id;

    try {
      if (text === '/start') {
        const existingUser = await UserModel.findOne({ where: { chatId } });
        if (!existingUser) {
          const newUser = await UserModel.create({ chatId });
          console.log('Новый пользователь создан:', newUser);
        } else {
          console.log('Пользователь уже существует:', existingUser);
        }
        const welcomeMessage = `
👋 Добро пожаловать в нашего бота! Вот что вы можете сделать:

📘 **Доступные команды:**
- /start — Посмотреть все команды.
- /test — Начать тестирование по выбранной главе.
- /info — Посмотреть информацию о ваших результатах и прогрессе.


🎉 Удачи и приятного времяпрепровождения!
        `;
        return bot.sendMessage(chatId, welcomeMessage, {
          parse_mode: 'Markdown',
        });
      }

      if (text === '/info') {
        try {
          const user = await UserModel.findOne({ where: { chatId } });
          if (!user) return bot.sendMessage(chatId, 'Пользователь не найден.');

          // Достаем testResults из пользователя
          const testResults = user.testResults || {}; // Если testResults не существует, то пустой объект

          if (Object.keys(testResults).length === 0) {
            return bot.sendMessage(chatId, 'Вы еще не прошли ни одного теста.');
          }

          // Формируем строку с информацией по всем главам
          let resultsMessage = 'Информация о ваших успехах:\n';

          // Проходим по каждой главе
          for (const [chapter, result] of Object.entries(testResults)) {
            resultsMessage += `\nГлава ${chapter}:\n`;

            // Если для главы есть результат, выводим его
            resultsMessage += `Лучший результат: ${result.score} из ${result.total} (Дата: ${result.date})\n`;
          }

          return bot.sendMessage(chatId, resultsMessage);
        } catch (error) {
          console.error('Ошибка при получении информации:', error);
          return bot.sendMessage(
            chatId,
            'Произошла ошибка при получении данных.'
          );
        }
      }

      if (text === '/test') {
        // Получаем все книги
        const books = await Books.findAll();
        if (!books || books.length === 0) {
          return bot.sendMessage(chatId, 'Книги не найдены.');
        }

        const buttons = books.map((book) => [
          { text: book.title, callback_data: `book_${book.id}` },
        ]);

        return bot.sendMessage(chatId, 'Выберите книгу для тестирования:', {
          reply_markup: { inline_keyboard: buttons },
        });
      }

      return bot.sendMessage(chatId, 'Команда не распознана.');
    } catch (e) {
      console.error('Ошибка обработки сообщения:', e);
      return bot.sendMessage(chatId, 'Произошла какая-то ошибка!');
    }
  });

  // Обработка callback-запросов
  bot.on('callback_query', async (msg) => {
    const chatId = msg.message.chat.id;
    const data = msg.data;

    try {
      // Подтверждаем обработку callback_query
      await bot.answerCallbackQuery(msg.id);

      // Ищем пользователя
      let user = await UserModel.findOne({ where: { chatId } });
      if (!user) {
        console.error('Пользователь не найден для chatId:', chatId);
        return bot.sendMessage(chatId, 'Ошибка: пользователь не найден.');
      }

      if (data.startsWith('answer_')) {
        const index = parseInt(data.split('_')[1], 10);

        if (!userAnswersMap.has(chatId)) {
          userAnswersMap.set(chatId, new Set());
        }
        const userAnswers = userAnswersMap.get(chatId);

        // Если вариант уже выбран, убираем его из списка, иначе добавляем
        if (userAnswers.has(index)) {
          userAnswers.delete(index);
        } else {
          userAnswers.add(index);
        }

        // Получаем вопросы для текущей главы
        const questionDataArray = await getQuestions(user.chapter);
        const questionData = questionDataArray[user.questionIndex].dataValues;

        // Обновляем клавиатуру с учётом выбранных вариантов
        let keyboard = questionData.options.map((option, idx) => {
          return [
            {
              text: userAnswers.has(idx) ? `✅ ${option}` : `${option}`,
              callback_data: `answer_${idx}`,
            },
          ];
        });

        // Добавляем кнопку для подтверждения ответов
        keyboard.push([
          { text: 'ПОДТВЕРДИТЬ ОТВЕТЫ', callback_data: 'confirm_answers' },
        ]);

        // Обновляем клавиатуру в сообщении
        await bot.editMessageReplyMarkup(
          {
            inline_keyboard: keyboard,
          },
          {
            chat_id: chatId,
            message_id: msg.message.message_id,
          }
        );
      } else if (data === 'confirm_answers') {
        const userAnswers = userAnswersMap.get(chatId);
        if (!userAnswers || userAnswers.length === 0) {
          return bot.sendMessage(
            chatId,
            'Вы должны выбрать один из вариантов ответа.'
          );
        }
        await checkAnswer(chatId, userAnswers, user);
        userAnswersMap.delete(chatId);
      } else if (data === 'next_question') {
        await handleNextQuestion(chatId);
      } else if (data.startsWith('book_')) {
        const bookid = data.split('_')[1];

        // Получаем главы для выбранной книги
        const chapters = await Questions.findAll({
          where: { bookid },
          attributes: ['chapter'],
          group: ['chapter'],
        });

        if (!chapters || chapters.length === 0) {
          return bot.sendMessage(chatId, 'Главы не найдены.');
        }

        // Множество для хранения уникальных глав
        const uniqueChapters = new Map();
        chapters.forEach((chapter) => {
          const cleanedChapter = chapter.chapter.trim();
          uniqueChapters.set(cleanedChapter, chapter);
        });

        // Сортировка глав
        const sortedChapters = Array.from(uniqueChapters.values()).sort(
          (a, b) => {
            const aParts = a.chapter.split('.').map(Number);
            const bParts = b.chapter.split('.').map(Number);
            for (let i = 0; i < Math.max(aParts.length, bParts.length); i++) {
              if ((aParts[i] || 0) < (bParts[i] || 0)) return -1;
              if ((aParts[i] || 0) > (bParts[i] || 0)) return 1;
            }
            return 0;
          }
        );

        const buttons = sortedChapters.map((chapter) => [
          { text: `Глава ${chapter.chapter}`, callback_data: chapter.chapter },
        ]);

        return bot.sendMessage(chatId, 'Выберите главу для теста:', {
          reply_markup: { inline_keyboard: buttons },
        });
      } else {
        // Обработка выбора главы
        const chapter = String(data);
        const questions = await Questions.findAll({
          where: { chapter },
          order: [['id', 'ASC']],
        });

        if (questions.length > 0) {
          user.chapter = chapter;
          user.questionIndex = 0;
          user.score = 0;
          user.answers = [];
          await user.save();
          return askQuestion(chatId, user);
        } else {
          console.error('Вопросы для главы не найдены:', chapter);
          return bot.sendMessage(
            chatId,
            'Ошибка: вопросы для главы не найдены.'
          );
        }
      }
      console.error('Неизвестное callback_data:', data);
    } catch (e) {
      console.error('Ошибка обработки callback_query:', e);
      bot.sendMessage(chatId, 'Произошла ошибка при обработке вашего запроса.');
    }
  });
};

start();
