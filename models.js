const sequelize = require('./db');
const { DataTypes } = require('sequelize');

const User = sequelize.define('user', {
  id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
  chatId: { type: DataTypes.INTEGER, unique: true },
  chapter: { type: DataTypes.STRING, defaultValue: null },
  score: { type: DataTypes.INTEGER, defaultValue: 0 },
  questionIndex: { type: DataTypes.INTEGER, defaultValue: 0 },
  answers: { type: DataTypes.ARRAY(DataTypes.TEXT), defaultValue: [] },
});
const Questions = sequelize.define(
  'questions',
  {
    chapter: DataTypes.TEXT,
    question: DataTypes.TEXT,
    options: DataTypes.ARRAY(DataTypes.TEXT),
    answer: DataTypes.ARRAY(DataTypes.INTEGER),
    citation: DataTypes.TEXT,
    question_type: DataTypes.TEXT,
    bookid: { type: DataTypes.INTEGER },
  },
  {
    timestamps: false,
  }
);
const Books = sequelize.define(
  'books',
  {
    title: DataTypes.TEXT,
  },
  {
    timestamps: false,
  }
);
async function getQuestions(chapter) {
  try {
    const questions = await Questions.findAll({
      where: {
        chapter: chapter,
      },
    });
    return questions;
  } catch (error) {
    console.error('Ошибка при получении вопросов:', error);
    throw error; // Re-throw the error for further handling (if necessary)
  }
}
async function getChapters() {
  try {
    const chapters = await Questions.findAll({
      attributes: ['chapter'],
      order: [['chapter', 'ASC']],
      raw: true, // Получаем только значения без лишней информации от Sequelize
    });

    // Преобразуем массив объектов в массив значений chapter
    const chapterValues = chapters.map((chapter) => chapter.chapter.trim());

    // Используем Set для получения уникальных значений без учета регистра и пробелов
    const uniqueChapters = [...new Set(chapterValues)];

    console.log('Unique chapters:', uniqueChapters);
    return uniqueChapters;
  } catch (error) {
    console.error('Ошибка при получении глав:', error);
    throw error; // Re-throw the error for further handling (if necessary)
  }
}
module.exports = { User, Questions, Books, getQuestions, getChapters };
