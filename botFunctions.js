const { bot } = require('./config');
const { getQuestions, User: UserModel } = require('./models');

async function askQuestion(chatId, user) {
  const questionDataArray = await getQuestions(user.dataValues.chapter);

  if (!questionDataArray || questionDataArray.length === 0) {
    console.error('No questions found:', questionDataArray);
    return bot.sendMessage(chatId, 'Нет данных по вопросам.');
  }

  const questionData = questionDataArray[user.questionIndex].dataValues;

  // Сначала отправим сообщение о прогрессе
  const progressMessage = `Вопрос ${user.questionIndex + 1} из ${questionDataArray.length}`;
  await bot.sendMessage(chatId, progressMessage);

  // Затем отправим сам вопрос
  await bot.sendMessage(chatId, questionData.question);

  // После вопроса отправим варианты ответов
  const optionsMessage = questionData.options
    .map((option, index) => `${index + 1}. ${option}`)
    .join('\n');
  await bot.sendMessage(chatId, optionsMessage);

  // Формируем клавиатуру с номерами вариантов ответов и кнопкой для подтверждения
  let keyboard = questionData.options.map((option, index) => [
    { text: `Вариант ${index + 1}`, callback_data: `answer_${index}` },
  ]);

  keyboard.push([
    { text: 'ПОДТВЕРДИТЬ ОТВЕТЫ', callback_data: 'confirm_answers' },
  ]);

  const options = {
    reply_markup: JSON.stringify({ inline_keyboard: keyboard }),
  };

  // Отправляем кнопки
  await bot.sendMessage(chatId, 'Выберите вариант ответа:', options);
}

async function handleAnswerSelection(chatId, selectedIndex) {
  try {
    let userDB = await UserModel.findOne({ where: { chatId } });
    if (!userDB) {
      console.error('User not found:', chatId);
      return bot.sendMessage(chatId, 'Пользователь не найден.');
    }
    if (!userDB.answers) {
      userDB.answers = [];
    }
    // Если ответ не добавлен, то добавляем его в массив
    userDB.answers = [selectedIndex];
    await userDB.save();

    bot.sendMessage(
      chatId,
      `Ответ ${selectedIndex} добавлен. Вы можете добавить другие ответы или подтвердить выбор.`
    );
  } catch (error) {
    console.error('Ошибка при сохранении ответа:', error);
    bot.sendMessage(chatId, 'Вам нужно выбрать один из ответов');
  }
}

async function checkAnswer(chatId, userAnswers, user) {
  const questionDataArray = await getQuestions(user.dataValues.chapter);
  const questionData = questionDataArray[user.questionIndex].dataValues;
  const correctAnswers = questionData.answer;

  // Проверка корректности ответа
  const isCorrect =
    correctAnswers.every((val) => userAnswers.has(val)) &&
    userAnswers.size === correctAnswers.length;

  const resultMessage = isCorrect
    ? `Правильно! ${questionData.citation}`
    : `Неправильно! ${questionData.citation}`;
  const options = { parse_mode: 'HTML' };
  await bot.sendMessage(chatId, resultMessage, options);

  user.score += isCorrect ? 1 : 0;
  user.questionIndex++;
  user.answers = []; // Очистка временных ответов после проверки

  try {
    await user.save();
  } catch (error) {
    console.error('Ошибка при сохранении пользователя:', error);
  }

  if (user.questionIndex < questionDataArray.length) {
    askQuestion(chatId, user);
  } else {
    const chapterKey = user.dataValues.chapter;
    const finalScore = user.score;
    const totalQuestions = questionDataArray.length;

    // Достаем текущие результаты пользователя
    const currentResults = user.testresults ? { ...user.testresults } : {};

    if (
      !currentResults[chapterKey] ||
      finalScore > currentResults[chapterKey].score
    ) {
      currentResults[chapterKey] = {
        score: finalScore,
        total: totalQuestions,
        date: new Date().toISOString(),
      };
    }

    user.set('testresults', currentResults);
    user.set({
      chapter: null,
      questionIndex: 0,
      score: 0,
    });

    try {
      await user.save();
    } catch (error) {
      console.error('Ошибка при сохранении результатов:', error);
    }

    const bestScore = currentResults[chapterKey].score;
    const bestTotal = totalQuestions;

    await bot.sendMessage(
      chatId,
      `Тест окончен! Ваш результат: ${finalScore} из ${totalQuestions}.\nЛучший результат: ${bestScore} из ${bestTotal}. Вы можете пройти тест еще раз /test`
    );
  }
}

module.exports = { askQuestion, checkAnswer, handleAnswerSelection };
